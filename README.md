# osu!blood
Firefox and Chrome extensions to quickly download the currently viewed osu! beatmapset via bloodcat's mirror. 

* Download for Firefox: https://addons.mozilla.org/en-US/firefox/addon/osu-blood/
* Download for Chrome: https://chrome.google.com/webstore/detail/osublood/olclgajblgcblpjahgjloielinoohlon

Created by https://osu.ppy.sh/u/WoctorDho (feel free to hmu in game)
## TODO:
* fix chrome version
* add support for old site
