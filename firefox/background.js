function onCreated(tab) {
  console.log(`Created new tab: ${tab.id}`)
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.pageAction.onClicked.addListener((tab) => {
	var base = "https://bloodcat.com/osu/s/"
	var beatmapid = tab.url.slice(31,37)
	var newurl = base.concat(beatmapid)
	console.log(tab.url, "converted to", newurl)
	var creating = browser.tabs.create({
		url:newurl
    });
	creating.then(onCreated, onError);
});